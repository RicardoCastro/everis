import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  @Input() private personagem: {};
  // @Input() private message: string;

  constructor(public ativarModal: NgbActiveModal) { }
}
