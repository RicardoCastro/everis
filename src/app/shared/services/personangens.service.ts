import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Personangens } from '../domains/personagens';


@Injectable()
export class PersonagensService {

    constructor(public _http: HttpClient) {
    }

    getPersonagens(url): Observable<Personangens> {
        return this._http.get<Personangens>(url)
        .map(dadosPersonagens => new Personangens(dadosPersonagens));
    }
}

