import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreRoutingModule } from './core.routing.module';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PersonagensService } from '../shared/services/personangens.service';
import { PersonagensComponent } from './personagens/personagens.component';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    HeaderComponent,
    PersonagensComponent,
  ],
  exports: [
    RouterModule,
  ],
  providers: [
    PersonagensService
  ],
})
export class CoreModule { }
