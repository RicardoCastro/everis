
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/*Component*/

import { PersonagensComponent } from './personagens/personagens.component';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'personagens',
        pathMatch: 'full'
    },
    {
        path: 'personagens',
        component: PersonagensComponent
    },
];



@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class CoreRoutingModule {

}
