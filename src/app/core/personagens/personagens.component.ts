import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../shared/components/modal/modal.component';
import { Personangens } from '../../shared/domains/personagens';
import { PersonagensService } from '../../shared/services/personangens.service';
import { APP_API } from '../../app.api';





@Component({
  selector: 'app-personagens',
  templateUrl: './personagens.component.html',
  styleUrls: ['./personagens.component.scss']
})
export class PersonagensComponent implements OnInit {
  public modalRef: NgbModalRef;
  public url = `${APP_API}`;
  public personagensLista = [];
  public personagens: Personangens[];
  constructor(public personagensService: PersonagensService,
              public modalService: NgbModal) { }

  ngOnInit() {

    this.personagensService.getPersonagens(this.url)
      .subscribe(
        dadosPersonagens => {
          this.personagensLista = Object.keys(dadosPersonagens).map(function (key) {
            return dadosPersonagens[key];
          }
          );
          this.personagens = this.personagensLista;
        },
        err => console.log(err),
        () => console.log('Realizando busca de books')
      );
  }
  detalheModal(personagem) {
    console.log(personagem);
    this.modalRef = this.modalService.open(ModalComponent);
    this.modalRef.componentInstance.personagem = personagem;

  }

}
