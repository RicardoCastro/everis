import { BrowserModule } from '@angular/platform-browser';
import { NgModule, } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import { ModalComponent } from './shared/components/modal/modal.component';


@NgModule({
  imports: [
    BrowserModule,
    CoreModule,
    HttpClientModule,
    NgbModule.forRoot(),

  ],
  declarations: [
    AppComponent,
    ModalComponent
  ],
  exports: [
    ModalComponent,
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalComponent,
  ]
})
export class AppModule { }
